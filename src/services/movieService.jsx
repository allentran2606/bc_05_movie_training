import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";
export const movieService = {
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getPhimTheoHeThongRap: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP00`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
