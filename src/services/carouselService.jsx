import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";

export const carouselService = {
  getCarouselImg: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
