const initialState = {
  carouselImg: [
    // {
    //   maBanner: 1,
    //   maPhim: 1282,
    //   hinhAnh:
    //     "https://s3img.vcdn.vn/123phim/2021/04/ban-tay-diet-quy-evil-expeller-16177781815781.png",
    // },
    // {
    //   maBanner: 2,
    //   maPhim: 1282,
    //   hinhAnh:
    //     "https://s3img.vcdn.vn/123phim/2021/04/lat-mat-48h-16177782153424.png",
    // },
    // {
    //   maBanner: 3,
    //   maPhim: 1282,
    //   hinhAnh:
    //     "https://s3img.vcdn.vn/123phim/2021/04/nguoi-nhan-ban-seobok-16177781610725.png",
    // },
    // Do đang dùng initialState từ local nên có sẵn 3 object
    // Khi nào dùng carousel img từ api trả về thì comment lại => carouselImg[] (mảng rỗng)
  ],
};

export const carouselReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SET_CAROUSEL": {
      return { ...state, carouselImg: payload };
    }
    default:
      return state;
  }
};
