const initialState = {
  heThongCumRap: [],
};

export const cumRapReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SET_HE_THONG_RAP": {
      return { ...state, heThongCumRap: payload };
    }
    default:
      return { ...state };
  }
};
