import React from "react";
import { NavLink } from "react-router-dom";

export default function Blogs() {
  return (
    <div className="blogs mt-20">
      <div className="mb-4 border-b border-gray-200">
        <ul
          className="flex flex-wrap -mb-px font-medium text-center justify-center items-center"
          id="myTab"
          data-tabs-toggle="#myTabContent"
          role="tablist"
        >
          <li className="mr-2" role="presentation">
            <button
              className="inline-block p-4 border-b-2 rounded-t-lg text-2xl text-gray-800"
              id="profile-tab"
              data-tabs-target="#profile"
              type="button"
              role="tab"
              aria-controls="profile"
              aria-selected="false"
            >
              Điện ảnh 24h
            </button>
          </li>
          <li className="mr-2" role="presentation">
            <button
              className="inline-block p-4 border-b-2 rounded-t-lg hover:text-gray-600 hover:border-gray-300 text-2xl text-gray-800"
              id="dashboard-tab"
              data-tabs-target="#dashboard"
              type="button"
              role="tab"
              aria-controls="dashboard"
              aria-selected="false"
            >
              Review
            </button>
          </li>
          <li className="mr-2" role="presentation">
            <button
              className="inline-block p-4 border-b-2 rounded-t-lg hover:text-gray-600 hover:border-gray-300 text-2xl text-gray-800"
              id="settings-tab"
              data-tabs-target="#settings"
              type="button"
              role="tab"
              aria-controls="settings"
              aria-selected="false"
            >
              Khuyến mãi
            </button>
          </li>
        </ul>
      </div>

      <div id="myTabContent">
        {/* Tab 1 */}
        <div
          className="p-4 rounded-lg grid lg:grid-cols-3 lg:gap-4 text-justify"
          id="profile"
          role="tabpanel"
          aria-labelledby="profile-tab"
        >
          <div>
            <img
              src="https://s3img.vcdn.vn/123phim/2020/07/tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam-15959320391357.png"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                TENET công bố ngày khởi chiếu chính thức ở Việt Nam
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5 sm:mb-8">
              Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo
              chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường
              bên ngoài Bắc Mỹ, trong đó có Việt Nam.
            </p>
          </div>
          <div>
            <img
              src="https://s3img.vcdn.vn/123phim/2020/07/tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam-15959320391357.png"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                Bao lâu nữa thì bán được 1 tỷ gói mè? Trả lời
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5 sm:mb-8">
              Zǎo shang hǎo zhōng guó! Xiàn zài wǒ yǒu bing chilling Wǒ hěn xǐ
              huān bing chilling Dàn shì "sù dù yǔ jī qíng jiǔ" bǐ bing chilling
              "sù dù yǔ jī qíng, sù dù yǔ jī qíng jiǔ" Wǒ zuì xǐ huān Suǒ yǐ
              xiàn zài shì yīn yuè shí jiān Zhǔn bèi Yī, èr, sān
            </p>
          </div>
          <div>
            <img
              src="https://scontent.fsgn5-11.fna.fbcdn.net/v/t1.6435-9/197226403_591881698884116_7982268664999483584_n.jpg?stp=cp0_dst-jpg_e15_fr_q65&_nc_cat=110&ccb=1-7&_nc_sid=7aed08&_nc_ohc=QcI5pEDLMcQAX81297p&_nc_ht=scontent.fsgn5-11.fna&oh=00_AfBOID16zwuKrCWzi6oXFmoqmUeD2LPcvUImLjAOT34S7Q&oe=63E648A9"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                Tiết lộ mức lương khủng của sinh viên Bách Khoa sinh năm 96 học
                IT...
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5">
              Ngành IT Việt Nam hiện nay ở đầu của sự phát triển. Có thể nói IT
              là vua của các nghề. Vừa có tiền, có quyền. Vừa kiếm được nhiều $
              lại được xã hội trọng vọng. Thằng em mình học bách khoa cơ khí,
              sinh năm 96. Tự mày mò học code rồi đi làm remote cho công ty Mỹ 2
              năm nay. Mỗi tối online 3-4 giờ là xong việc. Lương tháng 3k6.
              Nhưng thu nhập chính vẫn là từ nhận các project bên ngoài làm
              thêm. Tuần làm 2,3 cái nhẹ nhàng 9,10k tiền tươi thóc thật không
              phải đóng thuế. Làm gần được 3 năm mà nhà xe nó đã mua đủ cả. Nghĩ
              mà thèm.
            </p>
          </div>
        </div>

        {/* Tab 2 */}
        <div
          className="p-4 rounded-lg grid lg:grid-cols-3 lg:gap-4 text-justify"
          id="dashboard"
          role="tabpanel"
          aria-labelledby="dashboard-tab"
        >
          <div>
            <img
              src="https://s3img.vcdn.vn/123phim/2020/07/khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan-15943683481617.jpg"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                TENET công bố ngày khởi chiếu chính thức ở Việt Nam
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5 sm:mb-8">
              Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo
              chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường
              bên ngoài Bắc Mỹ, trong đó có Việt Nam.
            </p>
          </div>
          <div>
            <img
              src="https://s3img.vcdn.vn/123phim/2020/07/khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan-15943683481617.jpg"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                TENET công bố ngày khởi chiếu chính thức ở Việt Nam
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5 sm:mb-8">
              Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo
              chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường
              bên ngoài Bắc Mỹ, trong đó có Việt Nam.
            </p>
          </div>
          <div>
            <img
              src="https://s3img.vcdn.vn/123phim/2020/07/khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan-15943683481617.jpg"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                TENET công bố ngày khởi chiếu chính thức ở Việt Nam
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5">
              Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo
              chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường
              bên ngoài Bắc Mỹ, trong đó có Việt Nam.
            </p>
          </div>
        </div>

        {/* Tab 3 */}
        <div
          className="p-4 rounded-lg grid lg:grid-cols-3 lg:gap-4 text-justify"
          id="settings"
          role="tabpanel"
          aria-labelledby="settings-tab"
        >
          <div>
            <img
              src="https://s3img.vcdn.vn/123phim/2020/07/dien-vien-dac-biet-cua-bang-chung-vo-hinh-15937518743844.png"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                TENET công bố ngày khởi chiếu chính thức ở Việt Nam
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5 sm:mb-8">
              Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo
              chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường
              bên ngoài Bắc Mỹ, trong đó có Việt Nam.
            </p>
          </div>
          <div>
            <img
              src="https://s3img.vcdn.vn/123phim/2020/07/dien-vien-dac-biet-cua-bang-chung-vo-hinh-15937518743844.png"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                TENET công bố ngày khởi chiếu chính thức ở Việt Nam
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5 sm:mb-8">
              Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo
              chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường
              bên ngoài Bắc Mỹ, trong đó có Việt Nam.
            </p>
          </div>
          <div>
            <img
              src="https://s3img.vcdn.vn/123phim/2020/07/dien-vien-dac-biet-cua-bang-chung-vo-hinh-15937518743844.png"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                TENET công bố ngày khởi chiếu chính thức ở Việt Nam
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5">
              Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo
              chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường
              bên ngoài Bắc Mỹ, trong đó có Việt Nam.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
