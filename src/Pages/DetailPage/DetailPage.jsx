import React from "react";
import { useParams } from "react-router-dom";

export default function DetailPage() {
  let param = useParams();
  // Lấy id của 1 bộ phim khi ấn vào detail
  return <div>{param.id}</div>;
}
