import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import MultipleRowsTablet from "./MultipleRowsTablet";
import MultipleRowsMobile from "./MultipleRowsMobile";

export default function FilmList({ movieArr }) {
  return (
    <div>
      <Desktop>
        <MultipleRowsTablet movieArr={movieArr} />
      </Desktop>
      <Tablet>
        <MultipleRowsTablet movieArr={movieArr} />
      </Tablet>
      <Mobile>
        <MultipleRowsMobile movieArr={movieArr} />
      </Mobile>
    </div>
  );
}
