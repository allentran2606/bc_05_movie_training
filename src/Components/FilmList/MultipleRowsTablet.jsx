import React, { Component } from "react";
import Slider from "react-slick";
import FilmCardTablet from "./FilmCardTablet";
import styleSlick from "./MultipleRowSlick.module.css";

export default class MultipleRows extends Component {
  renderMovieListTablet = () => {
    this.props.movieArr.pop();
    return this.props.movieArr.map((item, index) => {
      return (
        <div key={index} className={`${styleSlick["width-item"]} px-2 py-2`}>
          <FilmCardTablet film={item} />
        </div>
      );
    });
  };

  render() {
    const settings = {
      className: "center variableWidth",
      centerMode: true,
      infinite: true,
      centerPadding: "0px",
      slidesToShow: 3,
      speed: 300,
      rows: 1,
      slidesPerRow: 2,
      variableWidth: true,
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      autoplay: true,
    };

    return (
      <div>
        <Slider {...settings}>{this.renderMovieListTablet()}</Slider>
      </div>
    );
  }
}
