import React, { Component } from "react";
import Slider from "react-slick";
import FilmCardMobile from "./FilmCardMobile";
import styleSlick from "./MultipleRowSlick.module.css";

export default class MultipleRowsMobile extends Component {
  renderMovieListMobile = () => {
    this.props.movieArr.pop();
    return this.props.movieArr.map((item, index) => {
      return (
        <div key={index} className={`${styleSlick["width-item"]}`}>
          <FilmCardMobile film={item} />
        </div>
      );
    });
  };

  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      vertical: true,
      verticalSwiping: true,
      swipeToSlide: true,
      autoplay: true,
    };
    return (
      <div>
        <Slider {...settings}>{this.renderMovieListMobile()}</Slider>
      </div>
    );
  }
}
